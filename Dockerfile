FROM golang:alpine as run-template
WORKDIR /src
ENV COMMAND="$COMMAND"
ENV HOST="$HOST"
ENV UDP_PORT="$UDP_PORT"
ENV ID="$ID"
ENV HTTP_PORT="$HTTP_PORT"


COPY . ./
RUN go mod tidy
RUN go build 
