package server

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"

	"github.com/Ishan27g/gossipProtocol"
)

const (
	Version = "v1"
)

var (
	// gossip
	host    = "host.docker.internal"
	updPort = ""
	id      = ""

	// http
	httpPort = ""
	joined   = false
)

type GossipReq struct {
	Message string `json:"message"`
}
type JoinReq struct {
	Address string `json:"address"`
	Id      string `json:"id"`
}
type Server struct {
	g       gossipProtocol.Gossip
	receive <-chan gossipProtocol.Packet
}

func anyEmpty(vals ...string) bool {
	for _, str := range vals {
		if str == "" {
			return true
		}
	}
	return false
}

func endRequest(w *http.ResponseWriter, status int, body []byte) {
	(*w).WriteHeader(status)
	(*w).Write(body)
}

func (s *Server) joinOrAddHandler(w http.ResponseWriter, r *http.Request) {
	bytes, err := io.ReadAll(r.Body)
	if err != nil {
		endRequest(&w, http.StatusBadRequest, []byte("Bad request"))
		return
	}
	var jr JoinReq
	if err := json.Unmarshal(bytes, &jr); err != nil {
		endRequest(&w, http.StatusBadRequest, []byte("Bad json"))
		return
	}
	peer := gossipProtocol.Peer{UdpAddress: jr.Address, ProcessIdentifier: jr.Id}
	if joined {
		s.g.Add(peer)
		endRequest(&w, http.StatusAccepted, []byte(fmt.Sprintf("Added :%+v\n", peer)))
		return
	}
	joined = true
	s.g.Join(peer)
	endRequest(&w, http.StatusAccepted, []byte(fmt.Sprintf("Joined network & Added :%+v\n", peer)))
	return
}

func (s *Server) gossip(w http.ResponseWriter, r *http.Request) {
	bytes, err := io.ReadAll(r.Body)
	if err != nil {
		endRequest(&w, http.StatusBadRequest, []byte("Bad request"))
		return
	}
	var gr GossipReq
	if err := json.Unmarshal(bytes, &gr); err != nil {
		endRequest(&w, http.StatusBadRequest, []byte("Bad json"))
		return
	}

	s.g.SendGossip(gr.Message)
	endRequest(&w, http.StatusAccepted, []byte(fmt.Sprintf("Sent message :%s\n", gr.Message)))
	return
}
func Run() {
	if os.Getenv("HOSTNAME") != "" {
		host = os.Getenv("HOSTNAME")
	}
	httpPort = os.Getenv("HTTP_PORT")
	updPort = os.Getenv("UDP_PORT")
	id = os.Getenv("ID")
	if anyEmpty(host, updPort, id) {
		log.Fatal("missing env")
		return
	}
	r := mux.NewRouter()
	s := Server{
		g:       nil,
		receive: nil,
	}
	s.g, s.receive = gossipProtocol.Config(host, updPort, id)

	v := r.PathPrefix("/" + Version).Subrouter()
	{
		v.HandleFunc("/gossip", s.gossip).Methods("POST")
		v.HandleFunc("/join", s.joinOrAddHandler).Methods("POST")
	}
	go func() {
		for {
			gossipPacket := <-s.receive
			log.Println("data - ", gossipPacket.GossipMessage.Data)
			log.Println("event clock for this packet - ", gossipPacket.VectorClock)
		}
	}()
	log.Println("listening on " + httpPort)
	err := http.ListenAndServe(":"+httpPort, r)
	if err != nil {
		return
	}

}
