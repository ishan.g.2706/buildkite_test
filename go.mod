module gitlab.com/ishan.g.2706/buildkite_test

go 1.18

require (
	github.com/Ishan27g/gossipProtocol v0.0.0-20220411045301-d05ac7cf8d74
	github.com/gorilla/mux v1.8.0
	github.com/joho/godotenv v1.4.0
)

require (
	github.com/Ishan27g/go-utils/mLogger v0.0.0-20220308132511-ad5f5c0e8f14 // indirect
	github.com/Ishan27g/vClock v0.0.0-20220221133617-fae27884de39 // indirect
	github.com/emirpasic/gods v1.12.0 // indirect
	github.com/fatih/color v1.7.0 // indirect
	github.com/hashicorp/go-hclog v1.2.0 // indirect
	github.com/iancoleman/orderedmap v0.2.0 // indirect
	github.com/mattn/go-colorable v0.1.4 // indirect
	github.com/mattn/go-isatty v0.0.10 // indirect
	golang.org/x/sys v0.0.0-20191008105621-543471e840be // indirect
)
