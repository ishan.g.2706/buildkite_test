package main

import (
	"github.com/joho/godotenv"
	"gitlab.com/ishan.g.2706/buildkite_test/server"
)

func main() {
	godotenv.Load()
	server.Run()
}
